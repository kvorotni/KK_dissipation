function f = function_for_coefficients(x,a11,a12,a21,a22,h,y,m,k);
%the aim of this function is to find the values of the coefficients
%required for IRK next step solution (K1 and K2).
%a11,a12,a21,a22,h are the scheme parameters and step size respectively
%m - column vector of masses
%k - column vector of stiffnesses
%y - system solution at the current step
%x - two columns vector containing unknown values of K1 and K2

%calculation of residuals based on the scheme parameters' values, system
%parameters and step-size
f(:,1)= potential(y(:,1)+h*(a11*x(:,1)+a12*x(:,2)),m,k)-x(:,1);
f(:,2)= potential(y(:,1)+h*(a21*x(:,1)+a22*x(:,2)),m,k)-x(:,2);
end