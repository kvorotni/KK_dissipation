function f = function_of_IRK_scheme_solution(N,gamma_check,h,tfinal,m,k,y0,tolerance)
% N - number of elements (scalar)
% gamma_check - value of the dissipative term (scalar)
% h - value of step-size (scalar)
% tfinal - value of time denoting the simulation end (scalar)
% m - column vector of masses of the chain elements (vector)
% k - column vector of stiffnesses recorded in the contacts between the
% chain elements (vector)
% x0 - column vector of initial conditions of solution (vector)
% tolerance - the value of tolerance setting the accuracy of fsolve
% function

StepNumber = round(tfinal/h);   %calculation of stepnumber required for numerical solution

%calculation of the scheme parameters with respect to the value of the
%dissipative term and stability constraints
C11 = gamma_check/(2*h);
s = -1/sqrt(3);
C = -C11;

beta_scheme = sqrt(3*C11*(1+2*C11));
alpha_scheme = sqrt(3)*C11*(2+5/2*C11)+1/2*beta_scheme;
b1 = 1/2+beta_scheme;

a11 = C11/2+1/4-C/2+alpha_scheme;
a22 = C11/2+1/4-C/2-alpha_scheme;
a12 = a22+C+(1-b1)*s;
a21 = a11+C-b1*s;

%calculation of the coefficients appearing in the higher order terms, we
%need these values to calculate the velocity values
C22 = 2*(b1*a11+(1-b1)*a21)*(a11+a12)+2*(b1*a12+(1-b1)*a22)*(a21+a22)-1/3;
C32 = 6*(a11+a12)*(b1*(a11^2+a12*a21)+(1-b1)*a21*(a11+a22))+6*(a21+a22)*(b1*a12*(a11+a22)+(1-b1)*(a22^2+a12*a21))-1/4;
C33 = 3*(a11+a12)^(2)*(b1*a11+(1-b1)*a21)+3*(a21+a22)^(2)*(b1*a12+(1-b1)*a22)-1/4;

options =  optimoptions('fsolve','MaxFunEvals',2000,'MaxIter',2000,'TolFun',tolerance);
%setting the options for fsolve function

%initialization of numerical solution
j = 1;          %first point of solution
t(j) = 0;       %first value of time of solution
y(:,j) = y0;    %first solution step

%solution routine
while (j<=StepNumber)
   [KK,fval] = fsolve(@(X)(coefficients(X,a11,a12,a21,a22,h,y(:,j),m,k)),1*[potential(y(:,j),m,k),potential(y(:,j),m,k)],options);
   %finding the values of coefficients for calculating the next step solution 
   %using the two-stage IRK scheme approach with respect to the options settled above
    
   %recording the obtained solution of coefficients
   K1 = KK(:,1);
   K2 = KK(:,2);
   
   %recording the next step solution based on the results of the
   %coefficients obtained above
   y(:,j+1) = y(:,j)+h*(b1*K1+(1-b1)*K2);
   %recording the value related to the next step
   t(j+1) = t(j)+h;
   %moving to the next point of solution
   j = j + 1;
end

%calculating the velocity values for each element; the calculation is based
%on the numerical scheme results
 velocity(1,:) = y(2,:)-C11*k(2,ones(1,length(t))).*max(y(1,:)-y(3,:),0).^(3/2)*h./m(1,ones(1,length(t)))...
     +(-3/4*C22*k(2,ones(1,length(t))).*y(2,:).*max(y(1,:)-y(3,:),0).^(1/2)./m(1,ones(1,length(t)))...
     +3/4*C22*k(2,ones(1,length(t))).*y(4,:).*max(y(1,:)-y(3,:),0).^(1/2)./m(1,ones(1,length(t))))*h^2;
 
 velocity(2:1:N-1,:) = y(4:2:2*N-2,:)+1./m(2:1:N-1,ones(1,length(t))).*h*C11.*(k(2:1:N-1,ones(1,length(t))).*max(y(1:2:2*N-5,:)...
     -y(3:2:2*N-3,:),0).^(3/2)-k(3:1:N,ones(1,length(t))).*max(y(3:2:2*N-3,:)-y(5:2:2*N-1,:),0).^(3/2))...
     +3/4*h^2*C22*1./m(2:1:N-1,ones(1,length(t))).*(k(2:1:N-1,ones(1,length(t))).*max(y(1:2:2*N-5,:)...
     -y(3:2:2*N-3,:),0).^(1/2).*(y(2:2:2*N-4,:)-y(4:2:2*N-2,:))-k(3:1:N,ones(1,length(t))).*max(y(3:2:2*N-3,:)...
     -y(5:2:2*N-1,:),0).^(1/2).*(y(4:2:2*N-2,:)-y(6:2:2*N,:)));
 
 velocity(N,:) = y(2*N,:)+h*C11*k(N,ones(1,length(t)))./m(N,ones(1,length(t))).*max(y(2*N-3,:)-y(2*N-1,:),0).^(3/2)...
     +3/4*h^2*C22*k(N,ones(1,length(t)))./m(N,ones(1,length(t))).*max(y(2*N-3,:)-y(2*N-1,:),0).^(1/2).*(y(2*N-2,:)-y(2*N,:));
 
%output of the function, first column is related to the time values of solution,
%remain columns contain the values of displacement and velocity of each
%chain element 
f(1,1:length(t))=t;
f(2:2:2*N,1:length(t))=y(1:2:2*N-1,:);
f(3:2:2*N+1,1:length(t))=velocity(1:1:N,:);
end