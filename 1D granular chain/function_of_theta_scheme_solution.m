function f=function_of_theta_scheme_solution(N,gamma_check,h,tfinal,m,k,x0,tolerance)
% N - number of elements (scalar)
% gamma_check - value of the dissipative term (scalar)
% h - value of step-size (scalar)
% tfinal - value of time denoting the simulation end (scalar)
% m - column vector of masses of the chain elements (vector)
% k - column vector of stiffnesses recorded in the contacts between the
% chain elements (vector)
% x0 - column vector of initial conditions of solution (vector)
% tolerance - the value of tolerance setting the accuracy of fsolve
% function

StepNumber = round(tfinal/h); %calculation of stepnumber required for numerical solution

theta = gamma_check/(2*h)+1/2; %calculation of theta value for numerical solution

%initialization of numerical solution
j = 1;              %first point of solution
t_discrete(j)=0;    %first value of time of solution
x(:,j) = x0;        %first solution step

options =  optimoptions('fsolve','MaxFunEvals',2000,'MaxIter',2000,'TolFun',tolerance); 
%setting the options for fsolve function

%solution routine
while (j<=StepNumber)
[x_next_step,fval] = fsolve(@(X)(X-x(:,j)-h*theta*potential(X,m,k)-h*(1-theta)*potential(x(:,j),m,k)),x(:,j),options);
%finding the solution for next step using the theta scheme approach with
%respect to the options settled above

x(:,j+1) = x_next_step;             %recording the current solution value in solution array
t_discrete(j+1) = t_discrete(j)+h;  %recording the value related to the next step
j = j + 1;                          %moving to the next point of solution
end

%calculating the velocity values for each element; the calculation is based
%on the numerical scheme results
velocity(1,:) = x(2,:)-1./m(1,ones(1,length(t_discrete)))*h*(theta-1/2).*(-k(2,ones(1,length(t_discrete))).*max(x(1,:)-x(3,:),0).^(3/2));
velocity(2:1:N-1,:) = x(4:2:2*N-2,:)-1./m(2:1:N-1,ones(1,length(t_discrete)))*h*(theta-1/2).*(k(2:1:N-1,ones(1,length(t_discrete))).*max(x(1:2:2*N-5,:)-x(3:2:2*N-3,:),0).^(3/2)-k(3:1:N,ones(1,length(t_discrete))).*max(x(3:2:2*N-3,:)-x(5:2:2*N-1,:),0).^(3/2));
velocity(N,:) = x(2*N,:)-1./m(N,ones(1,length(t_discrete)))*h*(theta-1/2).*(k(N,ones(1,length(t_discrete))).*max(x(2*N-3,:)-x(2*N-1,:),0).^(3/2));

%output of the function, first column is related to the time values of solution,
%remain columns contain the values of displacement and velocity of each
%chain element
f(1,1:length(t_discrete))=t_discrete;
f(2:2:2*N,1:length(t_discrete))=x(1:2:end,:);
f(3:2:2*N+1,1:length(t_discrete))=velocity(1:1:end,:);
end