function f = potential(x,m,k);
%calculating the values of the displacements and velocities in the
%conservative chain

%x - column vector of displacements and velocities
%m - column vector of masses
%k - column vector of stiffnesses recorded in the contacts between the
% chain elements

N = length(m);      %length of the chain
f = zeros(2*N,1);   %initialization of the solution vector

%calculating the solution of first element
f(1,1) = x(2);
f(2,1) = 1/m(1,1)*(-k(2,1)*max(x(1)-x(3),0)^(3/2));

%calculating the solution of intermediate element 
f(3:2:2*N-3,1) = x(4:2:2*N-2);
f(4:2:2*N-2,1) = 1./m(2:1:N-1,1).*(k(2:1:N-1,1).*max(x(1:2:2*N-5)-x(3:2:2*N-3),0).^(3/2)-k(3:1:N,1).*max(x(3:2:2*N-3)-x(5:2:2*N-1),0).^(3/2));

%calculating the solution of last element
f(2*N-1,1) = x(2*N);
f(2*N,1) = 1/m(N,1)*(k(N,1)*max(x(2*N-3)-x(2*N-1),0)^(3/2))-0/m(N,1)*(k(N+1,1)*max(x(2*N-1),0)^(3/2));
end