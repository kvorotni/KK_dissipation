function f = potential_linear(x,m,K);
%calculating the contribution of the linear potentials on the
%values of the displacements and velocities in the
%conservative chain

%x - column vector of displacements and velocities
%m - column vector of masses
%K - column vector of stiffnesses' springs corresponding to the local
%potentials

N = length(m);      %length of the chain
f = zeros(2*N,1);   %initialization of the solution vector

%calculating the solution of first element
f(1,1) = 0;
f(2,1) = 1/m(1,1)*(-K(1,1)*x(1));

%calculating the solution of intermediate element 
f(3:2:2*N-3,1) = 0;
f(4:2:2*N-2,1) = 1./m(2:1:N-1,1).*(-K(2:1:N-1,1).*x(3:2:2*N-3));

%calculating the solution of last element
f(2*N-1,1) = 0;
f(2*N,1) = 1/m(N,1)*(-K(N,1)*x(2*N-1));
end